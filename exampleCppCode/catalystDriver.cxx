#include "catalystDataStructures.h"
#include <mpi.h>
#include <iostream>

// #ifdef USE_CATALYST
#include "catalystAdaptor.h"
// #endif

// Example of a C++ adaptor for a simulation code
// where the simulation code has a fixed topology
// grid. We treat the grid as an unstructured
// grid even though in the example provided it
// would be best described as a vtkImageData.
// Also, the points are stored in an inconsistent
// manner with respect to the velocity vector.
// This is purposefully done to demonstrate
// the different approaches for getting data
// into Catalyst. Note that through configuration
// that the driver can be run without linking
// to Catalyst.

// Put this into a functionObject in OpenFOAM

int main(int argc, char* argv[])
{
  MPI_Init(&argc, &argv);
  std::cout << "Starting Catalyst Driver: " << std::endl;
  Grid grid;
  unsigned int numPoints[3] = { 70, 60, 44 };
  double spacing[3] = { 1, 1.1, 1.3 };
  grid.Initialize(numPoints, spacing);
  Attributes attributes;
  attributes.Initialize(&grid);

// #ifdef USE_CATALYST
  // The first argument is the program name
  catalystAdaptor::Initialize(argc - 1, argv + 1);
// #endif
  unsigned int numberOfTimeSteps = 100;
  for (unsigned int timeStep = 0; timeStep < numberOfTimeSteps; timeStep++)
  {
    // use a time step length of 0.1
    double time = timeStep * 0.1;
    std::cout << "Time Step: = " << time << std::endl;
    attributes.UpdateFields(time);
// #ifdef USE_CATALYST
    catalystAdaptor::CoProcess(grid, attributes, time, timeStep, timeStep == numberOfTimeSteps - 1);
// #endif
  }

// #ifdef USE_CATALYST
  catalystAdaptor::Finalize();
// #endif
  MPI_Finalize();

  return 0;
}
