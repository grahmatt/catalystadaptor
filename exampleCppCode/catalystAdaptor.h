// Adaptor code

#ifndef CATALYSTADAPTOR_HEADER
#define CATALYSTADAPTOR_HEADER

class Attributes;
class Grid;

namespace catalystAdaptor
{
void Initialize(int numScripts, char* scripts[]);

void Finalize();

void CoProcess(
  Grid& grid, Attributes& attributes, double time, unsigned int timeStep, bool lastTimeStep);
}

#endif
